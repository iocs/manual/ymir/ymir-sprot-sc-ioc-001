require essioc
require ethercatmc

#Rotary stage, motor m4 only, now in NIN

epicsEnvSet("MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")

epicsEnvSet("IPADDR",        "$(SM_IPADDR=10.102.10.22)")
epicsEnvSet("IPPORT",        "$(SM_IPPORT=48898)")
epicsEnvSet("ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("PREFIX",        "YMIR-SpScn:MC-MCU-002:")
epicsEnvSet("PREC",          "$(SM_PREC=3)")
epicsEnvSet("SM_NOAXES",     "3")
epicsEnvSet("ECM_OPTIONS",   "adsPort=852;amsNetIdRemote=5.79.68.216.1.1;amsNetIdLocal=10.102.10.13.1.1"

# e3 common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")


# No poller yet, see ethercatmcStartPoller at the end of the script
epicsEnvSet("ECM_MOVINGPOLLPERIOD", "0")
epicsEnvSet("ECM_IDLEPOLLPERIOD", "0")
iocshLoad("$(ethercatmc_DIR)/ethercatmcController.iocsh")

#
# Axis 1
#
epicsEnvSet("AXISCONFIG",    "")
epicsEnvSet("PREFIX",        "YMIR-SpRot:MC-Rz-01:")
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=m)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=1)")
epicsEnvSet("DESC",          "$(SM_DESC=DESC)")
epicsEnvSet("EGU",           "$(SM_EGU=EGU)")
iocshLoad("$(ethercatmc_DIR)/ethercatmcIndexerAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)/ethercatmcAxisdebug.iocsh")
# Start polling, values are in millisconds
#
epicsEnvSet("MOVINGPOLLPERIOD", "10")
epicsEnvSet("IDLEPOLLPERIOD",   "100")
ethercatmcStartPoller("$(MOTOR_PORT)", "$(MOVINGPOLLPERIOD)", "$(IDLEPOLLPERIOD)")

iocinit()
